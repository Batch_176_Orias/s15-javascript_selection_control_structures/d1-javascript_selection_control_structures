console.log("Welcome Marvin!");

let num1 = true + 1;
console.log(num1);
console.log(typeof num1);

let num2 = false + 1;
console.log(num2);

//comparison operators
let name1 = `juan`;

console.log( 1 == `1`);
console.log(1 == `2`);
console.log(0 == false);
console.log(`juan` == name1);
console.log(2 != 2);
console.log(2 !=3);
console.log(0 != false);

console.log(1 === `1`);
console.log(0 === false);
console.log(2 === 2);
console.log(name1 === `Juan`);
console.log(2 !== 10);

// logical operators
let isT = true;
let isF = false;
let isR = isT && isF;
console.log(isR);
isR = isT || isF;
console.log(isR);
isR = !isR;
console.log(isR);
isR = !(isT || isF && isF);
console.log(isR);

// control structures
// if else

let num3 = 2;

if(num3 > 10){
	console.log(`Good`);
}
else{
	console.log(`Not good`);
}

//else if

let num4 = 2;

if(num4 == 1){
	console.log("Finally got it!");
}
else if(num4 == 2){
	console.log(`First place!`);
}
else{
	console.log(`This is a learning process`);
}

/*let shot = prompt(`Enter your age`);

if(shot < 19){
	console.log(`Still minor`);
}
else{
	console.log(`Drink ahead`);
}*/

// mini act

/*let height = prompt(`Enter height in cm`);

if(height < 150){
	console.log(`Did not pass the minimum height requirement`);
}
else{
	console.log(`Passed the minimum requirement`);
}
*/
/*function heightDisplay(height){
	if(height < 150){
		console.log(`Did not pass the minimum height requirement`);
	}
	else{
		console.log(`Passed the minimum requirement`);
	}
}

heightDisplay(prompt(`Enter your height in cm:`));*/

/*function height(h){
	if(h < 150){
		return `Did not pass the minimum height requirement`;
	}
	else{
		return `Passed the minimum requirement`;
	} 
}

console.log(height(prompt(`Enter height`)));*/

// truthsy and falsy
if(true){
	console.warn(`Truthy`);
}
else{
	console.error(`Falsy`);
}

// ternary operator
let ternary = (1 < 18) ? `This is true` : `This is false`;
console.warn(ternary);

let tern1 = (1 === 5) ? console.log(`Not equal`) : console.log(`Equal`);

/*function tern(){
	let age = (prompt(`Enter age:`) > 20) ? `Adult` : `Young`;
	console.log(age);
}

tern();*/

/*function adult(){
	return `Adult`;
}

function minor(){
	return `Minor`;
}

function ternary1(a){
	return age = (prompt(`Enter age`) > 20) ? adult() : minor();
}

console.log(ternary1());*/

// function ternary2(a){
// 	return age = (prompt(`Enter age`) > 20) ? `Go ahead` : `Not allowed`;
// }

// console.log(ternary2());

// case switch

let day = `Monday`

switch(day){
	case `Monday`:
	console.log(`It is Monday`);
	break;
	case `Tuesday`:
	console.log(`It is Tuesday`);
	break;
	case `Wednesday`:
	console.log(`It is Wednesday`);
	break;
	case `Thursday`:
	console.log(`It is Thursday`);
	break;
	case `Friday`:
	console.log(`It is Friday`);
	break;
	case `Saturday`:
	console.log(`It is Saturday`);
	break;
	case `Sunday`:
	console.log(`It is Sunday`);
	break;
	default:
	console.log(`Invalid`);
	break;
}

function showAlert(windspeed){
	try{
		alerat(determineTyphoonIntensity(windspeed));
	}
	catch(error){
		console.warn(error.message);
	}
	finally{
		console.log(`proceed`);
	}
}

showAlert(50);